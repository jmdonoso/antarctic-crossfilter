//# dc.js Getting Started and How-To Guide
'use strict';

/* global dc,d3,crossfilter */

// ### Create Chart Objects

// Create chart objects associated with the container elements identified by the css selector.
// Note: It is often a good idea to have these objects accessible at the global scope so that they can be modified or
// filtered by other page controls.
var tipoCentralPieChart = dc.pieChart('#tipo-central-chart');
var porcentajeENRCPieChart = dc.pieChart('#porcentaje-enrc-chart');
var generacionHoraSemanaHeatChart = dc.heatMap('#hora-semana-chart');
var centralaBarChart = dc.rowChart('#central-chart');
var empresaBarChart = dc.rowChart('#empresa-chart');
var generacionLineChart = dc.lineChart('#generacion-line-chart');
var generacionMultiLineChart = dc.compositeChart("#generacion-serie-chart");
var generacionBarChart = dc.barChart('#generacion-bar-chart');
var categoriasSunburstChart = dc.sunburstChart('#yearly-bubble-chart');
//var nasdaqCount = dc.sunburstChart('.dc-data-count');
var textFilter = dc.textFilterWidget("#search");
var dataTotalTableChart = dc.dataTable('.dc-data-table');
var porcentajeENRCValueChart = dc.numberDisplay('#porcentaje-enrc-value-chart');

var cen;
d3.tsv('generacion_centrales_full_3.csv').then(function (data) {
  var dateFormatSpecifier = '%Y-%m-%d %H:%M:%S';
  var dateFormat = d3.timeFormat(dateFormatSpecifier)
  var dateFormatParser = d3.timeParse(dateFormatSpecifier);
  var numberFormat = d3.format('.2f');

  data.forEach(function (d) {
    d.dd = dateFormatParser(d.fecha);
    d.mes = d3.timeMonth(d.dd);
    d.dia = d3.timeDay(d.dd);
    d.generacion = +d.generacion;
    d.energia_ernc = +d.energia_ernc;
  });

  cen = crossfilter(data);
  var all = cen.groupAll();

  var moveDays = cen.dimension(function (d) {
    return d.dia;
  });
  var generacionXdia = moveDays.group().reduceSum(function (d) {
    return d.generacion;
  });
  var generacionERNCXdia = moveDays.group().reduceSum(function (d) {
    return d.energia_ernc;
  });
  var tipoCentral = cen.dimension(function (d) {
    return d.tipo_central;
  });
  var generacionXtipoCentral = tipoCentral.group().reduceSum(function (d) {
    return d.generacion;
  });
  var allGen = cen.groupAll().reduceSum(function(d) {
	  return d.generacion;
  });
  var allErnc = cen.groupAll().reduceSum(function(d) {
	  return d.energia_ernc;
  });
  var gainOrLoss = cen.dimension(function (d) {
        return d.energia_ernc > 0 ? 'SI' : 'NO';
  });
    // Produce counts records in the dimension
  var gainOrLossGroup = gainOrLoss.group();
  
  var erncPerformanceGroup = gainOrLoss.group().reduceSum(function(d) {
    if (d.key == 'SI') {
	  return d.energia_ernc;
	} else {
	  return d.generacion;
	};
  });
  
  var valErnc2 = cen.groupAll().reduce(
    function (p, v) {
      p.tot += v.generacion;
	  p.enernc += v.energia_ernc;
	  return p;
	},
	function (p, v) {
	  p.tot -= v.generacion;
	  p.enernc -= v.energia_ernc;
	  return p;
	},
	function () {
	  return {
	    tot: 0,
	    enernc: 0
	  };
	}
  );
  
  var valErncPerfomance = function(d) {
	return d.enernc ? d.enernc / d.tot * 100 : 0;
  };
  
  var genEnrcNormal =moveDays.group().reduceSum(function (d) {
	  return d.generacion - d.energia_ernc;
  });
  var tableData = cen.dimension(function (d) {
      return [d.central, d.empresa, d.tipo_central];
  });
  var dimension = cen.dimension(function (d) {
	  return [d.grupo, d.empresa, d.central];
  });
  var group = dimension.group().reduceSum(function (d) {
    return d.generacion;
  });
  var empresa = cen.dimension(function (d) {
    return d.empresa;
  });
  var generacionXempresa = empresa.group().reduceSum(function (d) {
    return d.generacion;
  });
  var central = cen.dimension(function (d) {
    return d.central;
  });
  var generacionXcentral = central.group().reduceSum(function (d) {
    return d.generacion;
  });  
  var searchText = cen.dimension(function (d) {
            return d.central + ' ' + d.empresa + ' ' + d.tipo_central;
  });
  var hourOfDay = cen.dimension(function (d) {
        return [+d.dd.getHours(),];
    });
  var dayOfWeekAndHour = cen.dimension(function(d) {
        var day = d.dd.getDay();
        var name = ['7.Dom', '1.Lun', '2.Mar', '3.Mie', '4.Jue', '5.Vie', '6.Sab'];
        return [+d.dd.getHours(),name[day]];
    });
  var generacionXHoraXDia = dayOfWeekAndHour.group().reduceSum(function (d) {
    return d.generacion;
  });
  
  var i = 0;
  
  categoriasSunburstChart
	.width(500)
    .height(500)
	.innerRadius(80)
	.dimension(dimension)
	.group(group)
	.valueAccessor(function (d) {
		return d.value;
	})
	.ordering(function(d){ return +d.value })
	.title(function (d) {
		return d.key +
		'\n'+Math.round(d.value) +' MWh (' +
		 Math.round(d.value / allGen.value() * 100) + '%)';
	})
	/*.label(function(d) {
		if (categoriasSunburstChart.hasFilter() && !categoriasSunburstChart.hasFilter(d.key)) {
			return d.key + '(0%)';
		}
		var label = d.key;
		if (allGen.value()) {
			label += ' (' + Math.round(d.value / allGen.value() * 100) + '%)';
		}
		return label;
	 })	*/
	;
  
  tipoCentralPieChart
    .width(230)
    .height(230)
	.innerRadius(35)
    //.radius(80)
	//.margins({top: 20, left: 10, right: 10, bottom: 20})
    .dimension(tipoCentral)
    .group(generacionXtipoCentral)
	//.elasticX(true)
	.title(function (d) {
		return d.key +": "+Math.round(d.value) + ' (' + Math.round(d.value / allGen.value() * 100) + '%)';
	})
	.label(function(d) {
			if (tipoCentralPieChart.hasFilter() && !tipoCentralPieChart.hasFilter(d.key)) {
                return d.key + '(0%)';
            }
            var label = d.key;
            if (allGen.value()) {
                label += ' (' + Math.round(d.value / allGen.value() * 100) + '%)';
            }
            return label;
    })
	//.xAxis().ticks(4)
	;
  porcentajeENRCPieChart
    .width(230)
    .height(230)
	.innerRadius(35)
    .dimension(gainOrLoss)
    .group(erncPerformanceGroup)
	.title(function (d) {
		return d.key +": "+Math.round(d.value) + ' (' + Math.round(d.value / allGen.value() * 100) + '%)';
	})
	.label(function(d) {
			if (porcentajeENRCPieChart.hasFilter() && !porcentajeENRCPieChart.hasFilter(d.key)) {
                return d.key + '(0%)';
            }
            var label = d.key;
            if (d.value) {
                label += ' (' + Math.round(d.value / allGen.value() * 100) + '%)';
            }
            return label;
    });
	
  porcentajeENRCValueChart
    .formatNumber(d3.format(".2f"))
    .group(valErnc2)
	.valueAccessor(valErncPerfomance)
	.html({
	  some:"<span style=\"font-size: 30px;\">%number</span> \%"
	});

  empresaBarChart /* dc.rowChart('#empresa-chart', 'chartGroup') */
	.width(280)
	.height(200)
	.margins({top: 20, left: 10, right: 10, bottom: 20})
	.group(generacionXempresa)
	.dimension(empresa)
    .elasticX(true)
	.cap(5)
	.title(function (d) {
		return d.key +": "+Math.round(d.value);
	})
	.xAxis().ticks(4)
	;
	
  centralaBarChart /* dc.rowChart('#empresa-chart', 'chartGroup') */
	.width(280)
	.height(200)
	.margins({top: 20, left: 10, right: 10, bottom: 20})
	.group(generacionXcentral)
	.dimension(central)
    .elasticX(true)
	.cap(5)
	.title(function (d) {
		return d.key +": "+Math.round(d.value);
	})
	.xAxis().ticks(4)
	;
  generacionHoraSemanaHeatChart /* dc.barChart('#volume-month-chart', 'chartGroup') */
	.width(40 * 24 + 80)
    .height(40 * 7 + 40)
    .dimension(dayOfWeekAndHour)
    .group(generacionXHoraXDia)
    .keyAccessor(function(d) { return +d.key[0]; })
    .valueAccessor(function(d) { return d.key[1]; })
    .colorAccessor(function(d) { return +d.value; })
	.label(function (d) {
            return d.key[1].split('.')[1];
        })
    .title(function(d) {
        return "Hora:   " + d.key[0] + "\n" +
               "Día:  " + d.key[1] + "\n" +
               "Generación: " + Math.round(d.value) + " MWh";})
    .colors(["#ffffd9","#edf8b1","#c7e9b4","#7fcdbb","#41b6c4","#1d91c0","#225ea8","#253494","#081d58"])
    .calculateColorDomain()
	.on('preRedraw', function() {
        generacionHoraSemanaHeatChart.calculateColorDomain();
    });
	  	
  generacionLineChart
    .renderArea(true)
    .width(990)
    .height(250)
    .transitionDuration(1000)
    .margins({top: 30, right: 50, bottom: 25, left: 50})
    .dimension(moveDays)
    .mouseZoomable(true)
    .rangeChart(generacionBarChart)
    .x(d3.scaleTime().domain([new Date(2019, 0, 1), new Date()]))
    .round(d3.timeDay.round)
    .xUnits(d3.timeDays)
    .elasticY(true)
    .renderHorizontalGridLines(true)
    .legend(dc.legend().x(800).y(10).itemHeight(13).gap(5))
    .brushOn(false)
    .group(generacionXdia, 'Generacion Real')
    .valueAccessor(function (d) {
      return d.value;
    })
	.stack(generacionERNCXdia, 'Generacion ERN', function (d) {
      return d.value;
    })
    .title(function (d) {
      var value = d.value.total ? d.value.total : d.value;
      if (isNaN(value)) {
        value = 0;
      }
      return dateFormat(d.key) + '\n' + numberFormat(value);
    });
	
  generacionMultiLineChart
    //.renderArea(true)
    .width(990)
    .height(200)
    //.chart(function(c) { return dc.lineChart(c).curve(d3.curveCardinal); })
	.rangeChart(generacionBarChart)
	.margins({top: 30, right: 50, bottom: 25, left: 50})
    .x(d3.scaleTime().domain([new Date(2019, 0, 1), new Date()]))
    .brushOn(false)
    .elasticY(true)
    .dimension(moveDays)
	.shareTitle(false)
    .compose([
        dc.lineChart(generacionMultiLineChart)
		.group(generacionERNCXdia, 'Generacion ENRC')
		.colors('#ef820d')
		.renderArea(true)
		.title(function (d) {
			var value = d.value.total ? d.value.total : d.value;
			if (isNaN(value)) {
				value = 0;
			}
			return dateFormat(d.key) + '\n' + numberFormat(value);
		})
		,
        dc.lineChart(generacionMultiLineChart)
		.group(generacionXdia, 'Generacion Real')
		.colors('#225ea8')
		.title(function (d) {
			var value = d.value.total ? d.value.total : d.value;
			if (isNaN(value)) {
				value = 0;
			}
			return dateFormat(d.key) + '\n' + numberFormat(value);
		})
		//.renderArea(true)
    ])
    .mouseZoomable(true)
	.renderHorizontalGridLines(true)
    //.seriesAccessor(function(d) {return "Expt: " + d.key[0];})
	.valueAccessor(function (d) {
        return d.value;
    })
    //.keyAccessor(function(d) {return +d.key[1];})
    //.valueAccessor(function(d) {return +d.value;})
    //.legend(dc.legend().x(350).y(350).itemHeight(13).gap(5).horizontal(1).legendWidth(140).itemWidth(70))
	.legend(dc.legend().x(800).y(20).itemHeight(13).gap(5))
	
	;
  
  generacionBarChart /* dc.barChart('#generacion-bar-chart', 'chartGroup'); */
    .width(990) 
    .height(40)
    .margins({top: 0, right: 50, bottom: 20, left: 40})
    .dimension(moveDays)
    .group(generacionXdia)
    .centerBar(true)
    .gap(1)
    .x(d3.scaleTime().domain([new Date(2019, 0, 1), new Date()]))
    .round(d3.timeDay.round)
    .alwaysUseRounding(true)
    .xUnits(d3.timeDays)
	.elasticY(true)
	;
  
  textFilter.dimension(searchText);
  dataTotalTableChart /* dc.dataTable('.dc-data-table', 'chartGroup') */
		.width(300)
        .height(480)
        .dimension(tableData)
        .size(Infinity)
        .showSections(false)
        .columns(['central','empresa','grupo','tipo_central','fecha','generacion'])
        .sortBy(function (d) {return d.dd;})
        .order(d3.descending)
        .on('preRender', update_offset)
        .on('preRedraw', update_offset)
        .on('pretransition', display)
		.on('renderlet', function (c) {
            i = 0;
        })
		;
	dc.renderAll();
});	
      var ofs = 0, pag = 17;
      function update_offset() {
          var totFilteredRecs = cen.groupAll().value();
          var end = ofs + pag > totFilteredRecs ? totFilteredRecs : ofs + pag;
          ofs = ofs >= totFilteredRecs ? Math.floor((totFilteredRecs - 1) / pag) * pag : ofs;
          ofs = ofs < 0 ? 0 : ofs;
          dataTotalTableChart.beginSlice(ofs);
          dataTotalTableChart.endSlice(ofs+pag);
      }
      function display() {
          var totFilteredRecs = cen.groupAll().value();
          var end = ofs + pag > totFilteredRecs ? totFilteredRecs : ofs + pag;
          d3.select('#begin')
              .text(end === 0? ofs : ofs + 1);
          d3.select('#end')
              .text(end);
          d3.select('#last')
              .attr('disabled', ofs-pag<0 ? 'true' : null);
          d3.select('#next')
              .attr('disabled', ofs+pag>=totFilteredRecs ? 'true' : null);
          d3.select('#size').text(totFilteredRecs);
          if(totFilteredRecs != cen.size()){
            d3.select('#totalsize').text("(Total: " + cen.size() + " )");
          }else{
            d3.select('#totalsize').text('');
          }
      }
      function next() {
          ofs += pag;
          update_offset();
          dataTotalTableChart.redraw();
      }
      function last() {
          ofs -= pag;
          update_offset();
          dataTotalTableChart.redraw();
      }
	  function reversible_group(group) {
		return {
			top: function(N) {
				return group.top(N);
			},
			bottom: function(N) {
				return group.top(Infinity).slice(-N).reverse();
			}
		};
	  }